<footer id="gtn-footer">


  <!--== copyright row starts ==-->
  <div class="gtn-copyright">
    <div class="container">
      <div class="row">
        <!--== column 1 starts ==-->
        <div class="col-12 col-md-6">
          <!-- social starts -->
          <ul class="gtn-social gtn-social-list">
            <li>
              <a href="#" class="gtn-twitter" target="_blank" title="twitter"></a>
            </li>
            <li>
              <a href="#" class="gtn-facebook" target="_blank" title="facebook"></a>
            </li>
            <li>
              <a href="#" class="gtn-linkedin" target="_blank" title="linkedin"></a>
            </li>
          </ul>
          <!-- social ends -->
        </div>
        <!--== column 1 ends ==-->

        <!--== column 2 starts ==-->
        <div class="col-12 col-md-6 text-end small-device-space">
          <p class="warna-text">© 2022 TIM-BANGAN. All rights reserved</p>
        </div>
        <!--== column 2 ends ==-->
      </div>
    </div>
  </div>
  <!--== copyright row ends ==-->
</footer>