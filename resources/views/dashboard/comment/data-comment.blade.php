@extends('dashboard.layout.main')

@section('container')
<div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <h2 class="mb-2 page-title">Komentar</h2>
        @if (session()->has('success'))

          <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}<button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
           
        @endif
                               
        <div class="row my-4">
          <!-- Small table -->
          <div class="col-md-12">
            <div class="card shadow">
              <div class="card-body">
                <!-- table -->
                <table class="table datatables" id="dataTable-1">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Judul Artikel</th>
                      <th>Nama</th>
                      <th>Komentar</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $i = 1 ?>
                    @foreach ($Komen as $kmn)
                      <tr>
                        <td>{{ $i }}</td>
                          @foreach ($artikel as $art)
                            @if ($kmn->artikel_id === $art['id'])
                              <td>
                                {{ $art['nama'] }}
                              </td>
                            @endif  
                          @endforeach  
                        <td>{{ $kmn->nama }}</td>
                        <td>{{ $kmn->comment }}</td>
                        <td>
                          <div class="row flex align-items-center justify-content-start">
                            @if ($kmn->is_allow === 0)
                              <form action="{{ url('/dashboard/data-Komen/'.$kmn->id) }}" method="POST">
                                @csrf
                                <button class="btn mb-2 mr-2 btn-success text-white">Setuju</button>
                              </form>
                            @else
                              <form action="{{ url('/dashboard/data-Komen/'.$kmn->id) }}" method="POST">
                                @method('patch')
                                @csrf
                                <button class="btn mb-2 mr-2 btn-danger text-white">Batal Setuju</button>
                              </form>
                            @endif
                            
                            <a href="{{ url('/dashboard/data-Komen/edit/'.$kmn->id) }}" class="btn mb-2 mr-2 btn-warning text-white">Detail</a>
                            <form action="{{ url('/dashboard/data-Komen/'.$kmn->id) }}" method="POST">
                              @method('delete')
                              @csrf
                              <button class="btn mb-2 btn-danger" onclick="return confirm('yakin hapus data?')">Hapus</button>
                            </form>
                        </div>
                        </td>
                      </tr>
                    
                    <?php $i+=1 ?>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div> <!-- simple table -->
        </div> <!-- end section -->
      </div> <!-- .col-12 -->
    </div> <!-- .row -->
  </div>
@endsection