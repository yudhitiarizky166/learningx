@extends('layout.main')

@section('container')

<section id="blog" class="gtn-section gtn-py-100 bg-white">
    <div class="container">
        <div class="row">
            <div class="artikel-header">
            </div>
            <diV class="my-3">
                <h2>Teks Orasi, Resurrection of Education Movement!</h2>
                <img class="img-fluid rounded-16 my-2" src="https://www.balairungpress.com/wp-content/uploads/2020/03/1.-IMG_0018-widya-scaled.jpg" />
                <div class="mb-3">
                    <ul class="text-secondary">
                        <li class="d-inline"><img class="me-2" width="20px" src="https://www.svgrepo.com/show/404906/bust-in-silhouette.svg" />Indra Sutarjo</li>
                        <li class="d-inline"><img class="mx-2" width="20px" src="https://www.svgrepo.com/show/404912/calendar.svg" />Senin, 12 Oktober 2022</li>
                    </ul>
                </div>
                <div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus. Proin in risus sed ipsum aliquet hendrerit vel ut lorem. Donec tincidunt magna elit, non iaculis urna auctor vitae. Mauris non dolor sed justo tincidunt volutpat. In suscipit felis vitae augue suscipit dictum. Fusce eget pellentesque tortor, ac bibendum nisi. Nunc at diam venenatis, eleifend felis in, elementum enim. Nam interdum neque mauris, et interdum neque vestibulum viverra. Duis quis elit vel nisl dictum tincidunt. Proin et nunc ut nunc sollicitudin tristique. Sed tincidunt libero et turpis iaculis laoreet. Mauris eu tellus sit amet ipsum lobortis aliquet.
                    </p>
                    <p>
                        In vel orci lacinia nisi laoreet accumsan quis id sem. Morbi porttitor malesuada magna vel gravida. Quisque ac ante fringilla, condimentum velit vitae, sodales ex. Etiam porttitor porttitor justo at volutpat. Nullam nec rutrum elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Etiam rhoncus bibendum metus, vitae rutrum nisl. Praesent vestibulum mauris felis, at ultrices velit facilisis nec. Nunc ut quam magna. Duis odio nulla, finibus sed pellentesque non, rhoncus ut sem. Morbi et augue eu lacus porta suscipit. Sed sit amet condimentum tortor. Phasellus ut risus nunc. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus vitae arcu faucibus, sollicitudin ex et, efficitur nunc.
                    </p>
                    <p>
                        Donec ac dui eros. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus vitae ipsum orci. Aenean pharetra risus metus. Praesent luctus pharetra libero, eu fermentum felis venenatis quis. Fusce at augue ut mauris tempus hendrerit ornare nec arcu. Cras dapibus metus nec massa vestibulum vehicula in sit amet nunc. Aenean porttitor sed dolor et interdum. Pellentesque consequat, odio ut faucibus semper, diam eros sagittis tellus, sed euismod ex lectus sit amet massa. Nullam rhoncus vehicula fringilla. Ut ut dolor diam. Vestibulum tempus libero ut nulla tincidunt pulvinar. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nulla tortor risus, consequat vel dolor sed, efficitur ornare erat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                    </p>
                    <p>
                        Nam mattis, sapien vitae accumsan facilisis, dui odio volutpat sapien, vitae mattis justo leo quis leo. Nunc in lorem ac turpis bibendum cursus quis eu odio. Etiam gravida, lacus et gravida semper, est ligula iaculis sem, quis tincidunt nisi est id dui. Nunc scelerisque quam non fermentum tincidunt. Nunc imperdiet tristique lorem, eu posuere nisi aliquet et. Pellentesque vestibulum leo laoreet, dapibus ligula in, imperdiet lectus. Etiam eu semper orci, quis commodo felis. Maecenas aliquam elit non nunc sagittis, a ultricies diam sagittis. Morbi molestie ornare faucibus. Curabitur et dui ut justo laoreet finibus et sit amet odio. Nullam pretium nibh sed elementum placerat.
                    </p>
                    <p>
                        Ut lacus nibh, lobortis sit amet placerat volutpat, feugiat sit amet lectus. Nulla malesuada, nunc ut commodo aliquam, felis dui ultrices dui, vitae auctor nulla nulla ut arcu. Nullam metus justo, varius sit amet sem ut, fringilla rutrum purus. Phasellus eleifend ligula orci, sit amet feugiat eros accumsan at. Nulla venenatis eleifend egestas. Cras molestie ligula a ex vulputate, a varius enim aliquet. Sed in pulvinar purus. Aliquam erat volutpat. Quisque id augue mattis, consectetur urna non, vestibulum magna. Etiam lacinia tincidunt augue rutrum facilisis. Pellentesque vel nulla tempor, facilisis ligula sit amet, ultrices arcu. Pellentesque imperdiet condimentum ipsum, id placerat enim scelerisque eu.
                    </p>
                </div>
            </diV>
        </div>
        <hr>
        <h5>Tuliskan Komentar Anda</h5>
        <hr>
        <div>
            <div class="input-group mb-3">
                <span class="input-group-text" id="basic-addon1">Nama Lengkap</span>
                <input type="text" class="form-control" placeholder="Nama Lengkap" aria-label="Username" aria-describedby="basic-addon1">
            </div>
            <div class="input-group">
                <span class="input-group-text">Komentar</span>
                <textarea class="form-control" aria-label="With textarea" placeholder="Tuliskan pendapat anda mengenai artikel..."></textarea>
            </div>
            <button type="button" class="btn btn-primary">Kirim Komentar</button>
        </div>
        <hr>
        <div>
            <div class="row my-5 px-3">
                <div class="col-1  ">
                    <img class="rounded-16" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRCnOmo9Hty9WtXvSyBe9-SsSFI86VhInK_Q&usqp=CAU" />
                </div>
                <div class="col-11 ">
                    <strong>Aditya Nurwahyudi</strong>
                    <span class="text-secondary small text-justify">12 Januari 2022, 08: 00</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.</p>
                </div>
            </div>
            <div class="row my-5 px-3">
                <div class="col-1">
                    <img class="rounded-16" src="https://media-exp1.licdn.com/dms/image/C5603AQGOZv2-nwmZHA/profile-displayphoto-shrink_800_800/0/1633584755603?e=2147483647&v=beta&t=8NLAe6pjB__WlG5k7IWfFSYsBpTcjzpZKo684bDmUIg" />
                </div>
                <div class="col-11">
                    <strong>Citra Bella</strong>
                    <span class="text-secondary small">12 Januari 2022, 08: 00</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.</p>
                </div>
            </div>
            <div class="row my-5 px-3">
                <div class="col-1">
                    <img class="rounded-16" src="https://media-exp1.licdn.com/dms/image/C4D03AQFXdfE8ye4KMA/profile-displayphoto-shrink_800_800/0/1612304329244?e=2147483647&v=beta&t=RzRYzpkUIF6rYOdP302gdW9n8jBWKtF6xJJj7OMp07Y" />
                </div>
                <div class="col-11">
                    <strong>Gilbert Sebastian</strong>
                    <span class="text-secondary small">12 Januari 2022, 08: 00</span>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse suscipit gravida est quis laoreet. Vestibulum nec felis consequat, dapibus elit nec, consectetur purus.</p>
                </div>
            </div>

        </div>
    </div>

</section>

@endsection