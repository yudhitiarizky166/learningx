<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Video;
use App\Models\Produk;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProdukController extends Controller
{
    protected $guarded = ['id'];

    public function index()
    {
        return view('belanja', [
            'title' => 'Belanja'
        ]);
    }

    public function dataproduk()
    {
        return view('dashboard.produk.dataproduk', [
            'title' => 'Data produk',
            'produk' => Produk::all(),
            'kategori' => Kategori::all(),
            'user' => User::all()
        ]);
    }

    public function tambahdataproduk()
    {
        return view('dashboard.produk.tambah-data-produk', [
            'title' => 'Data Video',
            'kategori' => Kategori::all(),
        ]);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'slug' => 'required|unique:artikels',
            'kategori_id' => 'required',
            'deskripsi' => 'required',
            'foto' => 'image|file|max:10240',
            'harga' => 'required',
            'berat' => 'required',
            'stok' => 'required'
        ]);

        $validatedData['user_id'] = auth()->user()->id;

        if ($request->file('foto')) {
            $validatedData['foto'] = $request->file('foto')->store('foto-produk');
        }

        Produk::create($validatedData);

        return redirect('/dashboard/data-produk')->with('success', 'Data berhasil ditambahkan!');
    }

    public function edit($id)
    {
        $produk = DB::table('produks')->where('id', $id)->first();

        return view('dashboard/produk/edit', [
            'title' => 'Edit Data produk',
            'produk' => $produk,
            'kategori' => Kategori::all(),
        ]);
    }

    public function editdataproduk(Request $request, $id)
    {
        $validatedData =  [
            'nama' => $request['nama'],
            'kategori_id' => $request['kategori_id'],
            'deskripsi' => $request['deskripsi'],
            'slug' => $request['slug'],
            'stok' => $request['stok'],
            'harga' => $request['harga'],
            'berat' => $request['berat']
        ];


        Produk::where('id', $id)->update($validatedData);
        return redirect('/dashboard/data-produk')->with('success', 'produk berhasil diubah');
    }

    public function delete($id)
    {

        DB::table('produks')->where('id', $id)->delete();

        return redirect('/dashboard/data-produk')->with('success', 'produk berhasil dihapus');
    }
}