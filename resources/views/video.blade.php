@extends('layout.main')

@section('container')

<section class="gtn-section gtn-py-100 my-5">
    <div class="container">
      <!--== row starts ==-->
      <div class="row">
        <!-- column 1 starts -->
        <div class="col-12 col-md-6">
          <img src="assets/images/video-page.png" alt="image" />
        </div>
        <!-- column 1 ends -->

        <!-- column 2 starts -->
        <div class="col-12 col-md-6 small-device-space">
          <!-- heading starts -->
          <div class="gtn-section-intro text-left">
            <div class="gtn-intro-subheading-wrapper">
              <p class="gtn-intro-subheading">Geo Tani</p>
            </div>
            <h2 class="gtn-intro-heading">
              Perbanyak<br />
              Ilmu anda disini<br />
              - Petani cermat
            </h2>
            <p class="gtn-intro-content">
              Saksikan beberapa video tentang pertanian seperti: Menanam,
              merawat, memanen dan lainnya
            </p>
          </div>
          <!-- heading ends -->

          <!-- button -->
          <a
            class="gtn-btn btn-blue gtn-px-lg gtn-mt-50"
            href="#video"
            role="button"
          >
            <span class="gtn-btn-text">Tonton Sekarang</span>
          </a>
        </div>
        <!-- column 2 ends -->
      </div>
      <!--== row ends ==-->
    </div>
  </section>
  <!-- section ends
================================================== -->

  <!-- services section starts
================================================== -->
  <section class="gtn-section gtn-pt-100 gtn-pb-70 bg-white" id="video">
    <div class="container">
      <!-- heading starts -->
      <div class="gtn-section-intro text-left gtn-mb-50">
        <div class="gtn-intro-subheading-wrapper">
          <p class="gtn-intro-subheading">Rekomendasi</p>
        </div>
        <h2 class="gtn-intro-heading">Untukmu dan tanamanmu</h2>
        <p class="gtn-intro-content">video rekomendasi hanya untukmu</p>
      </div>
      <!-- heading ends -->

      <!--== row starts ==-->
      <div class="row">
        <!-- column 1 starts -->
        <div class="col-6 col-md-6 col-lg-4">
          <!-- member starts -->
          <div
            class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
          >
            <div class="gtn-team-content-wrapper gtn-shadow">
              <div class="gtn-post-img">
                <img src="assets/images/teh.jpg" alt="image" />
              </div>
              <div
                class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                style="min-height: 150px"
              >
                <span class="gtn-meta-category"><a href="#">Teh</a></span>
                <h5 class="gtn-post-title">
                  <a href="detail-page/detail-video.html" rel="bookmark"
                    >Bagaimana cara menanam pohon teh dengan baik</a
                  >
                </h5>
              </div>
            </div>
          </div>
          <!-- member ends -->
        </div>
        <!-- column 1 ends -->

        <!-- column 1 starts -->
        <div class="col-6 col-md-6 col-lg-4">
          <div
            class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
          >
            <div class="gtn-team-content-wrapper gtn-shadow">
              <div class="gtn-post-img">
                <a href="detail-page/detail-video.html">
                  <img src="assets/images/video-3.png" alt="image"
                /></a>
              </div>
              <div
                class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                style="min-height: 150px"
              >
                <span class="gtn-meta-category"><a href="#">Teh</a></span>
                <h5 class="gtn-post-title">
                  <a href="detail-page/detail-video.html" rel="bookmark"
                    >Cara Pengolahan setelah teh dipanen</a
                  >
                </h5>
              </div>
            </div>
          </div>
        </div>
        <!-- column 1 ends -->

        <!-- column 1 starts -->
        <div class="col-6 col-md-6 col-lg-4">
          <!-- member starts -->
          <div
            class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
          >
            <div class="gtn-team-content-wrapper gtn-shadow">
              <div class="gtn-post-img">
                <img src="assets/images/video-4.png" alt="image" />
              </div>
              <div
                class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                style="min-height: 150px"
              >
                <span class="gtn-meta-category"><a href="#">Teh</a></span>
                <h5 class="gtn-post-title">
                  <a href="detail-page/detail-video.html" rel="bookmark"
                    >Penanaman dan pemilihan bibit terbaik</a
                  >
                </h5>
              </div>
            </div>
          </div>
          <!-- member ends -->
        </div>
        <!-- column 1 ends -->
      </div>
      <!--== row ends ==-->
    </div>
  </section>
  <!-- services section ends
================================================== -->

  <!-- process section starts
================================================== -->
  <section id="process" class="gtn-section gtn-py-100 bg-white">
    <div class="container">
      <!-- heading starts -->
      <div class="gtn-section-intro text-center gtn-mb-50">
        <div class="gtn-intro-subheading-wrapper">
          <p class="gtn-intro-subheading">Video</p>
        </div>
        <h2 class="gtn-intro-heading">Geotani untuk petani</h2>
        <p class="gtn-intro-content">
          Simak beberapa video lainnya yang dapat memberikan pengetahuan
          yang sebelumnya <br />
          belum anda dapatkan
        </p>
      </div>
      <!-- heading ends -->

      <!--== row starts ==-->
      <div class="row justify-content-center">
        <!-- process bar starts -->
        <div
          class="gtn-process-bar-center gtn-mb-70 gtn-pt-50 color-dark m-auto"
        >
          <div>
            <a
              href="#"
              class="gtn-twitter"
              target="_blank"
              title="twitter"
            ></a>
          </div>
          <div class="gtn-text-input gtn-mt-20">
            <input
              type="text"
              name="search"
              class="gtn-input-text"
              placeholder="Cari Video yang anda inginkan"
            />
            <button type="submit" class="gtn-text-btn"></button>
          </div>
        </div>
        <!-- process bar ends -->

        <!--== row starts ==-->
        <div class="row justify-content-center">
          <!-- column 1 starts -->
          <div class="col-6 col-md-6 col-lg-4">
            <!-- member starts -->
            <div
              class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
            >
              <div class="gtn-team-content-wrapper gtn-shadow">
                <div class="gtn-post-img">
                  <img src="assets/images/teh.jpg" alt="image" />
                </div>
                <div
                  class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                  style="min-height: 150px"
                >
                  <span class="gtn-meta-category"
                    ><a href="#">Teh</a></span
                  >
                  <h5 class="gtn-post-title">
                    <a href="detail-page/detail-video.html" rel="bookmark"
                      >Bagaimana cara menanam pohon teh dengan baik</a
                    >
                  </h5>
                </div>
              </div>
            </div>
            <!-- member ends -->
          </div>
          <!-- column 1 ends -->

          <!-- column 1 starts -->
          <div class="col-6 col-md-6 col-lg-4">
            <!-- member starts -->
            <div
              class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
            >
              <div class="gtn-team-content-wrapper gtn-shadow">
                <div class="gtn-post-img">
                  <img src="assets/images/gandum-100.jpg" alt="image" />
                </div>
                <div
                  class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                  style="min-height: 150px"
                >
                  <span class="gtn-meta-category"
                    ><a href="#">Gandum</a></span
                  >
                  <h5 class="gtn-post-title">
                    <a href="detail-page/detail-video.html" rel="bookmark"
                      >Cara bertani gandum seperti di rusia dan ukraina</a
                    >
                  </h5>
                </div>
              </div>
            </div>
            <!-- member ends -->
          </div>
          <!-- column 1 ends -->

          <!-- column 1 starts -->
          <div class="col-6 col-md-6 col-lg-4">
            <!-- member starts -->
            <div
              class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
            >
              <div class="gtn-team-content-wrapper gtn-shadow">
                <div class="gtn-post-img">
                  <img src="assets/images/video-4.png" alt="image" />
                </div>
                <div
                  class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                  style="min-height: 150px"
                >
                  <span class="gtn-meta-category"
                    ><a href="#">Teh</a></span
                  >
                  <h5 class="gtn-post-title">
                    <a href="detail-page/detail-video.html" rel="bookmark"
                      >Penanaman dan pemilihan bibit terbaik</a
                    >
                  </h5>
                </div>
              </div>
            </div>
            <!-- member ends -->
          </div>
          <!-- column 1 ends -->
          <!-- column 1 starts -->
          <div class="col-6 col-md-6 col-lg-4">
            <!-- member starts -->
            <div
              class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
            >
              <div class="gtn-team-content-wrapper gtn-shadow">
                <div class="gtn-post-img">
                  <img src="assets/images/video-5.png" alt="image" />
                </div>
                <div
                  class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                  style="min-height: 150px"
                >
                  <span class="gtn-meta-category"
                    ><a href="#">Beras</a></span
                  >
                  <h5 class="gtn-post-title">
                    <a href="detail-page/detail-video.html" rel="bookmark"
                      >Penanaman bibit padi dengan cara terbaik</a
                    >
                  </h5>
                </div>
              </div>
            </div>
            <!-- member ends -->
          </div>
          <!-- column 1 ends -->

          <!-- column 1 starts -->
          <div class="col-6 col-md-6 col-lg-4">
            <!-- member starts -->
            <div
              class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
            >
              <div class="gtn-team-content-wrapper gtn-shadow">
                <div class="gtn-post-img">
                  <img src="assets/images/video-3.png" alt="image" />
                </div>
                <div
                  class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                  style="min-height: 150px"
                >
                  <span class="gtn-meta-category"
                    ><a href="#">Teh</a></span
                  >
                  <h5 class="gtn-post-title">
                    <a href="detail-page/detail-video.html" rel="bookmark"
                      >Cara Pengolahan setelah teh dipanen</a
                    >
                  </h5>
                </div>
              </div>
            </div>
            <!-- member ends -->
          </div>
          <!-- column 1 ends -->

          <!-- column 1 starts -->
          <div class="col-6 col-md-6 col-lg-4">
            <!-- member starts -->
            <div
              class="gtn-team gtn-team-social-onhover text-center gtn-team-offset-border gtn-box-rounded"
            >
              <div class="gtn-team-content-wrapper gtn-shadow">
                <div class="gtn-post-img">
                  <img src="assets/images/video-6.png" alt="image" />
                </div>
                <div
                  class="gtn-post-content p-2 pt-4 align-items-center justify-content-center d-flex"
                  style="min-height: 150px"
                >
                  <span class="gtn-meta-category"
                    ><a href="#">Ubi</a></span
                  >
                  <h5 class="gtn-post-title">
                    <a href="detail-page/detail-video.html" rel="bookmark"
                      >Memanen Ubi dengan jaminan kualitas bermutu
                      tinggi</a
                    >
                  </h5>
                </div>
              </div>
            </div>
            <!-- member ends -->
          </div>
          <!-- column 1 ends -->
        </div>
        <!--== row ends ==-->
      </div>
      <!--== row ends ==-->
    </div>
  </section>

@endsection