@extends('layout.main')

@section('container')

<section id="blog" class="gtn-section gtn-py-100 bg-white">
    <div class="container">
      <!-- heading starts -->
      <div class="gtn-section-intro text-center gtn-mb-50">
        <div class="gtn-intro-subheading-wrapper">
          <p class="gtn-intro-subheading">Berita &amp; Artikel</p>
        </div>
        <h2 class="gtn-intro-heading">Artikel Terkini dan Terbaru</h2>
        <p class="gtn-intro-content">
          AKSIIIII BOYYYYY
        </p>

        <!-- process bar starts -->
        <div class="gtn-process-bar-center gtn-mb-70 gtn-pt-50 color-dark m-auto">
          <div><a href="#" class="gtn-twitter" target="_blank" title="twitter"></a></div>
          <div class="gtn-text-input gtn-mt-20">
            <input type="text" name="search" class="gtn-input-text" placeholder="Cari artikel yang ingin anda tahu" />
            <button type="submit" class="gtn-text-btn"></button>
          </div>
        </div>
      </div>
      <!-- heading ends -->

      <!--== row starts ==-->
      <div class="row">

        <!-- column 3 starts -->
        <div class="col-6 col-md-4 col-sm-6 mb-5"">
          <div class="gtn-post-item">
            <div class="gtn-post-img"><img src="assets/images/artikel-1.png" alt="image" /></div>
            <div class="gtn-post-content">
              <span class="gtn-meta-category">Tips & Trick</span>
              <h5 class="gtn-post-title"><a href="detail-page/detail-artikel.html" rel="bookmark">Budidaya Tanaman Pangan</a></h5>
              <div class="hide">
                  <p class="gtn-post-excerpt">Budidaya tanaman pangan merupakan suatu kegiatan menanam tanaman yang menjadi sumber karbohidrat utama....</p>
                  <a class="gtn-read-more" href="detail-page/detail-artikel.html"><span class="gtn-read-more-content">Selengkapnya ...</span></a>
              </div>
            </div>
          </div>
        </div>
        <!-- column 3 ends -->
        
        <!-- column 3 starts -->
        <div class="col-6 col-md-4 col-sm-6 mb-5">
          <div class="gtn-post-item">
            <div class="gtn-post-img"><img src="assets/images/artikel-2.png" alt="image" /></div>
            <div class="gtn-post-content">
              <span class="gtn-meta-category">Pisang</span>
              <h5 class="gtn-post-title"><a href="detail-page/detail-artikel.html" rel="bookmark">Cara menanam Pohon pisang</a></h5>
              <div class="hide">
                  <p class="gtn-post-excerpt">Pisang adalah buah yang memiliki banyak manfaat untuk kesehatan. Kandungan nutrisi yang terdapat pada ...</p>
                  <a class="gtn-read-more" href="detail-page/detail-artikel.html"><span class="gtn-read-more-content">Selengkapnya ...</span></a>
              </div>
            </div>
          </div>
        </div>
        <!-- column 3 ends -->
        
        <!-- column 3 starts -->
        <div class="col-6 col-md-4 col-sm-6 mb-5">
          <div class="gtn-post-item">
            <div class="gtn-post-img"><img src="assets/images/artikel-3.png" alt="image" /></div>
            <div class="gtn-post-content">
              <span class="gtn-meta-category">Jahe</span>
              <h5 class="gtn-post-title"><a href="detail-page/detail-artikel.html" rel="bookmark">Tanamlah Jahemu dengan baik</a></h5>
              <div class="hide">
                  <p class="gtn-post-excerpt">Dalam Penanaman jahe, kamu harus memperhatikan beberapa hal. salah satunya yaitu pupuk dan ...</p>
                  <a class="gtn-read-more" href="detail-page/detail-artikel.html"><span class="gtn-read-more-content">Selengkapnya ...</span></a>
              </div>
            </div>
          </div>
        </div>
        <!-- column 3 ends -->

      </div>
      <!--== row ends ==-->
    </div>
  </section>

@endsection