@extends('dashboard.layout.main')

@section('container')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12">
            <h2 class="page-title">Tambah Data komentar</h2>
            <div class="card shadow mb-4">
                <div class="card-header">
                    <strong class="card-title">Form komentar</strong>
                </div>
                <div class="card-body">
                    <div class="col-md-6">
                            <div class="form-group mb-3">
                                <label for="nama">nama</label>
                                <h6 id="nama">{{ $komentar->nama }}</h6>
                            </div>
                            <div class="form-group mb-3">
                                <label for="simpleinput">email</label>
                                <h6 id="email">{{ $komentar->email }}</h6>
                            </div>
                            <div class="form-group mb-3">
                                <label for="comment">comment</label>
                            </div>
                    </div> 
                    <div class="col-md-6">
                                @foreach ($artikel as $art)
                                    @if ($komentar->artikel_id === $art->id)
                                        <div class="form-group mb-3">
                                            <label for="nama">Artikel</label>
                                            <h6 id="nama">{{ $art->nama }}</h6>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Dibuat pada</label>
                                            <h6 id="email">{{ $komentar->created_at }}</h6>
                                        </div>
                                    @endif
                                @endforeach
                            
                            <div class="form-group mb-3">
                                <label for="comment">comment</label>
                            </div>
                    </div> 
                </div> <!-- / .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> 
</div>


@endsection