<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Artikel;
use App\Models\Comment;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function datakomentar()
    {
        $id = auth()->user()->id;
        return view('dashboard.comment.data-comment', [
            'title' => 'Data Video',
            'Komen' => DB::table('comments')->where('user_id', $id)->get(),
            'kategori' => Kategori::all(),
            'user' => User::all(),
            'artikel' => Artikel::all()
        ]);
    }

    public function allow($id)
    {
        $is_allow = [
            'is_allow' => 1
        ];

        Comment::where('id', $id)->update($is_allow);
        return redirect('/dashboard/komentar')->with('success', 'Komentar telah disetujui');
    }

    public function not_allow($id)
    {
        $is_allow = [
            'is_allow' => 0
        ];

        Comment::where('id', $id)->update($is_allow);
        return redirect('/dashboard/komentar')->with('success', 'Komentar batal disetujui');
    }

    public function delete($id)
    {
        Comment::where('id', $id)->delete($id);
        return redirect('/dashboard/komentar')->with('success', 'Komentar telah dihapus');
    }

    public function store(Request $request)
    {
        $validatedData =  $request->validate([
            'nama' => 'required|max:255',
            'user_id' => 'required',
            'artikel_id' => 'required',
            'comment' => 'required',
            'email' => 'required|email'
        ]);
        $validatedData['is_allow'] = 0;

        Comment::create($validatedData);

        return redirect('/artikel/' . $request->artikel_id);
    }

    public function tambahdatakomentar()
    {
        return view('dashboard/comment/tambah-data-komen', [
            'title' => 'Tambah Data Komen'
        ]);
    }
}